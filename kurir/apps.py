from django.apps import AppConfig


class KurirConfig(AppConfig):
    name = 'kurir'
