from django.test import TestCase
from django.urls import resolve
from . import views
from django.http import HttpRequest
# Create your tests here.

class KurirTest(TestCase):
    def test_kurir_url_is_exist(self):
        response = self.client.get('/kurir/')
        self.assertEqual(response.status_code, 200)
    
    def test_chef_using_kurir_func(self):
        found = resolve('/kurir/')
        self.assertEqual(found.func, views.kurir)