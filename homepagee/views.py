from django.shortcuts import render, redirect
from django.http import HttpResponse
from homepage.models import Person1
from django.urls import reverse_lazy
from django.views.generic.edit import DeleteView
from homepagee.forms import PostForm
from homepagee.models import Post
# Create your views here.
def post_list(request):
    return render(request, 'post_list.html', {'postlist': Person1.objects.all()})
    
def feedback(request):
    postlist = Post.objects.all()
    return render(request, 'feedback.html', {'postlist2':postlist})
    
    
def post_new(request):
    if request.method == "POST":
           form = PostForm(request.POST)
           if form.is_valid():
               post = form.save()
               post.save()
               return redirect('/feedback/')
    else:
        form = PostForm()
    return render(request, 'bayar.html', {'form': form})
    
    
class Delete(DeleteView):
    model=Person1
    success_url=reverse_lazy('homepagee:post_list')
    