from django.contrib import admin
from django.urls import path, include
from chef import views

app_name = 'chef'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('chef/', views.chef, name='chef'),
    path('chef/done/<int:id>/', views.chef_done, name='chef_done'),
    path('chef/request/', views.bahan, name='bahan'),
  ]