from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from homepage.models import Person1
from .models import Bahan
from . import forms
# Create your views here.

def chef(request):
	response = {}
	pesanan1 = Person1.objects.all().order_by('date')
	response['pesanan_list'] = pesanan1
	return render(request, "chef.html", response)

def chef_done(request, id):
	if request.method == 'POST':
	    Person1.objects.filter(id=id).delete()
	    return redirect('chef:chef')
	else:
	    return HttpResponse("/GET not allowed")

def bahan(request):
	if request.method == 'POST':
		form = forms.RequestBahan(request.POST, request.FILES)
		if form.is_valid():
			instance = form.save(commit=False)
			form.save()
			return redirect('chef:chef')
	else:
		form = forms.RequestBahan()
	return render(request, 'request_bahan.html', {'form' : form})