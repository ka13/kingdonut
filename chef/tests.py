from django.test import TestCase
from django.urls import resolve
from . import views
from django.http import HttpRequest
# Create your tests here.

class ChefTest(TestCase):
    def test_chef_url_exist(self):
        #test url  '/' exist
        response = self.client.get('/chef/')
        self.assertEqual(response.status_code, 200)
    
    def test_chef_using_chef_page(self):
        #test url '/' will using landing page template
        response = self.client.get('/chef/')
        self.assertTemplateUsed(response, 'chef.html')
    
    def test_chef_calling_chef_views_function(self):
        found = resolve('/chef/')
        self.assertEqual(found.func, views.chef)
 