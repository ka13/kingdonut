from django import forms
from . import models
from django.utils import timezone
from datetime import datetime

class Time(forms.TimeInput):
    input_type = "time"

class Date(forms.DateInput):
    input_type = "date"
    input_formats = ['%d %b %Y']

class Person(forms.ModelForm):
    class Meta:
        model = models.Person1
        fields = ['name', 'number', 'address', 'date', 'donut_base', 'donut_flav_A', 'donut_flav_B', 'donut_flav_C', 'donut_flav_D', 'donut_flav_E', 'donut_flav_F', 'donut_flav_G', 'donut_flav_H']

        widgets = {
            "date": Date
        }