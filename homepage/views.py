from django.shortcuts import render, redirect
from . import forms
from .models import Person1

# Create your views here.
def home(request):
    return render(request, "home.html")

def person_create(request):
    if request.method == "POST":
        form = forms.Person(request.POST)

        if form.is_valid():   
            new_sched = form.save(commit=False)
            new_sched.save()
            return redirect('homepagee:post_list')
    else:
        form = forms.Person()

    return render(request, "schedule.html", {"form" : form, "person": Person1.objects.all().order_by("date")})

